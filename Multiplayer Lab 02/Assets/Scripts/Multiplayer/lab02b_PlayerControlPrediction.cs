﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public enum CharacterState
{
    Idle = 1,
    WalkingForward,
    WalkingBackwards,
    Jumping
}

public class lab02b_PlayerControlPrediction : NetworkBehaviour
{
    struct PlayerState
    {
        public CharacterState anim;
        public int movementNumber;
        public float posX, posY, posZ;
        public float rotX, rotY, rotZ;
    }

    [SyncVar(hook = "OnServerStateChanged")]
    PlayerState serverState;

    PlayerState predictedState;

    Queue<KeyCode> pendingMoves;

    CharacterState astate;
    public Animator animationthingy;

    [Server]
    void InitState()
    {
        serverState = new PlayerState
        {
            movementNumber = 0,
            posX = -119f,
            posY = 165.08f,
            posZ = -924f,
            rotX = 0f,
            rotY = 0f,
            rotZ = 0f
        };
    }

    void SyncState()
    {
        PlayerState stateToRender = isLocalPlayer ? predictedState : serverState;

        transform.position = new Vector3(stateToRender.posX, stateToRender.posY, stateToRender.posZ);
        transform.rotation = Quaternion.Euler(stateToRender.rotX, stateToRender.rotY, stateToRender.rotZ);
        animationthingy.SetInteger("CharacterState", (int)stateToRender.anim);
    }

    PlayerState Move(PlayerState previous, KeyCode newKey)
    {
        float deltaX = 0, deltaY = 0, deltaZ = 0;
        float deltaRotationY = 0;

        switch (newKey)
        {
            case KeyCode.Q:
                deltaX = -0.5f;
                break;
            case KeyCode.S:
                deltaZ = -0.5f;
                break;
            case KeyCode.E:
                deltaX = 0.5f;
                break;
            case KeyCode.W:
                deltaZ = 0.5f;
                break;
            case KeyCode.A:
                deltaRotationY = -1f;
                break;
            case KeyCode.D:
                deltaRotationY = 1f;
                break;
        }

        return new PlayerState
        {
            anim = CalcAnimations(deltaX, deltaY, deltaZ, deltaRotationY),
            movementNumber = 1 + previous.movementNumber,
            posX = deltaX + previous.posX,
            posY = deltaY + previous.posY,
            posZ = deltaZ + previous.posZ,
            rotX = previous.rotX,
            rotY = deltaRotationY + previous.rotY,
            rotZ = previous.rotZ
        };
    }

    // Use this for initialization
    void Start()
    {
        InitState();
        predictedState = serverState;
        if(isLocalPlayer)
        {
            pendingMoves = new Queue<KeyCode>();
            UpdatePredictedState();
        }
        SyncState();
    }

    // Update is called once per frame
    void Update()
    {
        if (isLocalPlayer)
        {
            Debug.Log(pendingMoves.Count);
            KeyCode[] possibleKeys = { KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.W, KeyCode.Q, KeyCode.E, KeyCode.Space };
            foreach (KeyCode possibleKey in possibleKeys)
            {
                if (!Input.GetKey(possibleKey))
                    continue;               //Then do nothing!

                pendingMoves.Enqueue(possibleKey);
                UpdatePredictedState();
                CmdMoveOnServer(possibleKey);
            }

            KeyCode[] movekeys = { KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.W, KeyCode.Q, KeyCode.E, KeyCode.Space };

            bool somethingpressed = false;

            foreach(KeyCode key in movekeys)
            {
                if (!Input.GetKey(key))
                    continue;
                somethingpressed = true;
                pendingMoves.Enqueue(key);
                UpdatePredictedState();
                CmdMoveOnServer(key);
            }

            if(!somethingpressed)
            {
                pendingMoves.Enqueue(KeyCode.Alpha0);
                UpdatePredictedState();
                CmdMoveOnServer(KeyCode.Alpha0);
            }
        }

        SyncState();
    }

    [Command]
    void CmdMoveOnServer(KeyCode pressedKey)
    {
        serverState = Move(serverState, pressedKey);
    }

    void OnServerStateChanged(PlayerState newState)
    {
        serverState = newState;
        if(pendingMoves != null)
        {
            while(pendingMoves.Count > (predictedState.movementNumber - serverState.movementNumber))
            {
                pendingMoves.Dequeue();
            }
            UpdatePredictedState();
        }
    }

    void UpdatePredictedState()
    {
        predictedState = serverState;
        foreach (KeyCode moveKey in pendingMoves)
            predictedState = Move(predictedState, moveKey);
    }

    CharacterState CalcAnimations(float x, float y, float z, float rot)
    {
        if (x==0&&y==0&&z==0)
            return CharacterState.Idle;
        if(x!=0||z!=0)
        {
            if (x>0||z>0)
            {
                return CharacterState.WalkingForward;
            }
            else
                return CharacterState.WalkingBackwards;
        }
        return CharacterState.Idle;
    }
}
